# DCNCa2 COSC1111  
DCNC assignment 2, Semester 2, 2015  
Part 2, programming a client & server  
  
###Group Members  
Shae Whitehead <s3502586@student.rmit.edu.au>  
Julien De-Sainte-Croix <s3369242@student.rmit.edu.au>  
Ashley Rowland <s3547126@student.rmit.edu.au>  
Robert Laine-Wyllie <s3433096@student.rmit.edu.au>  
  
## How To:  
Unzip assignment2.zip and use either Eclipse, or bash to run the server + client
###Eclispe:  
Import the project as file > import > existing java project, and select the highest directory (DCNCa2).  
Open the source files for Client.java, and Server.java  
Run the Server.java file first, and then run Client.java  
 1. Import project into Eclispe
 2. Run Server.java
 3. Run Client.java
 4. Send messages via the console of the client
  
###Command Line:  
Run the following, either as separate commands, or run the setup.sh script (The script just does all this for you):

Either 

bash setup.sh  
  
Or  
  
javac dateServer/src/Client.java -cp dateServer/src/ -d dateServer/bin/  
javac dateServer/src/Server.java -cp dateServer/src/ -d dateServer/bin/  
cd dateServer/bin  
xterm -title "Server" -e "java Server" &  
xterm -title "Client" -e "java Client" &  

And now you're free to send messages from client to server!  

Demo edit