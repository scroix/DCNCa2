import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

import utils.HammingHandler;

/**
 * A TCP server that runs on port 9090. When a client connects, it waits for the
 * client to send a string and decodes it, then closes the connection with that
 * client.
 */
public class Server {
	/**
	 * Runs the server.
	 * 
	 * Note: Remember to 'STOP' the program, and not just 'RUN' again over the
	 * top, this ensures that socket.close() and listener.close() run otherwise
	 * you'll get an 'Address already in use' exception.
	 * 
	 * (to fix) Unix Terminal : lsof -i:9090 : kill <whatever-the-pid-is>
	 */
	public static void main(String[] args) throws IOException {
		System.out
				.print("Welcome to a simple java server. This server will wait for a connection from a client, and will display strings the client sends\n");
		String message = "";
		int[][] returned = null;

		Socket clientSocket = null;
		ServerSocket serverSocket = null;
		ObjectInputStream is = null;

		PrintWriter out = null;
		BufferedReader in = null;

		serverSocket = new ServerSocket(9090);
		clientSocket = serverSocket.accept();
		System.out.print("Server listening at " + serverSocket
				+ "\nAwaiting message...\n");

		out = new PrintWriter(clientSocket.getOutputStream(), true);
		in = new BufferedReader(new InputStreamReader(
				clientSocket.getInputStream()));

		do {
			is = new ObjectInputStream(clientSocket.getInputStream());
			try {
				array = (int[][]) is.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			System.out.println("Recieved Hamming encode: ");
			System.out.println(Arrays.deepToString(array));

			System.out.println("Stripped of hamming bits: ");
			System.out
					.println(Arrays.deepToString(HammingHandler.decode(array)));

			returned = HammingHandler.decode(array);

			message = HammingHandler.english(returned);
			System.out.println(HammingHandler.english(returned));
			out.println(message);

		} while (message != "Bye.");

		// String inputLine;
		// while ((inputLine = in.readLine()) != null)
		// {
		// System.out.println("Server: " + inputLine);
		// out.println(inputLine);
		// if (inputLine.equals("Bye."))
		// break;
		// }
		/*
		 * Close all resources
		 */
		out.println("Now closing. Goodbye.");
		System.out.println("Server now closing. Goodbye.");
		out.close();
		in.close();
		clientSocket.close();
		serverSocket.close();
	}

	private static int[][] array = null;

}
