import java.io.*;
import java.net.*;

import utils.HammingHandler;

public class Client {

	public static void main(String[] args) throws IOException {
		Client client = new Client();

		// Initalise server and messaging
		String serverHostname = new String("127.0.0.1");
		Socket echoSocket = null;
		ObjectOutputStream out = null;
		BufferedReader in = null;

		// Establish connection to server
		echoSocket = new Socket(serverHostname, 9090);
		// out = new PrintWriter(echoSocket.getOutputStream(), true);
		out = new ObjectOutputStream(echoSocket.getOutputStream());
		in = new BufferedReader(new InputStreamReader(
				echoSocket.getInputStream()));
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));

		String client_input;
		int[][] client_binary; // 2d-array of input: [char location][binary
								// representation]
		int client_ham[][]; // 2d-array of input: [char location][hamming
							// encoded representation]

		System.out.println("Connection established: ");

		// Read entire message, convert to binary, convert to hamming, then send
		// to server.
		while ((client_input = stdIn.readLine()) != null) {

			// Local message as typed:
			System.out.println();
			System.out.println("Client (Clean): " + client_input);

			// Local message converted into a binary array:
			client_binary = client.convert(client_input);
			System.out.print("Client (Binary): ");
			for (int i = 0; i < client_binary.length; i++) {
				for (int j = 0; j < client_binary[i].length; j++) {
					System.out.print(client_binary[i][j]);
				}
				System.out.print(" ");
			}
			System.out.println();

			// Local messages converted to hamming code;
			System.out.print("Client (Hamming): ");
			client_ham = HammingHandler.convert(client_binary);
			for (int i = 0; i < client_ham.length; i++) {
				for (int j = 0; j < client_ham[i].length; j++) {
					System.out.print(client_ham[i][j]);
				}
				System.out.print(" ");
			}
			System.out.println();
			System.out.println();

			// Send the actual message in the form of a hamming encoded int
			// array to the server
			out.writeObject(client_ham);

			// Server returns message (copy of whatever it received)
			System.out.println("Server: " + in.readLine());
			System.out.println();

		}

		out.close();
		in.close();
		stdIn.close();
		echoSocket.close();
	}

	/****************************************************************************
	 * <p>
	 * CONVERT INPUT TO BINARY EQUIVALENT (AS STRING ARRAY)
	 * </p>
	 * <p>
	 * Later passed to another method to convert it to an integer array.
	 * </p>
	 * 
	 * @author Adapted from a Stack Overflow thread
	 ****************************************************************************/
	public int[][] convert(String message) {
		String preEncode = message;
		String binary_string[];
		int postEncode[][];

		// Convert to binary equivalent and add each char to a new string.
		byte[] bytes = preEncode.getBytes();
		StringBuilder binary = new StringBuilder();
		for (byte b : bytes) {
			int val = b;
			for (int i = 0; i < 8; i++) {
				binary.append((val & 128) == 0 ? 0 : 1);
				val <<= 1;
			}
			binary.append(' ');
		}

		// Split string into an array and pass to be converted again.
		binary_string = binary.toString().split(" ");

		postEncode = convert_charToNum(binary_string);

		return postEncode;
	}

	/****************************************************************************
	 * <p>
	 * CONVERT STRING ARRAY INTO EQUIVLAENT INT ARRAY
	 * </p>
	 * <p>
	 * One by one converts the string character (0,1) into a numerical version.
	 * </p>
	 ****************************************************************************/
	public int[][] convert_charToNum(String[] binary_string) {
		int[][] binary_array = new int[binary_string.length][8];

		// Arrays are the exact same size, so just one-for-one swapping.
		for (int j = 0; j < binary_string.length; j++) {
			for (int i = 0; i < binary_string[j].length(); i++) {
				binary_array[j][i] = binary_string[j].charAt(i) - '0';
			}
		}

		return binary_array;
	}

}