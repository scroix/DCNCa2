package utils;

import java.util.*;

/*
 * Return types are void as i wasn't sure what to return.
 * Also, no constructor.
 * */

public class HammingHandler {
	public static int[][] convert(int[][] in) {
		/* Method to add parity bits to int arrays */
		int[][] hamEncode = new int[in.length][12];
		for (int i = 0; i < in.length; i++) {
			/*
			 * Check math here. This definitely needs revision. See
			 * users.cis.fiu.edu/~downeyt/cop3402/hamming.html for reference
			 * material
			 * 
			 * pre hamEncode(): [[d0,d1,d2,d3,d4,d5,d6,d7][...]]
			 * 
			 * post hamEncode():
			 * [[r0,r1,d0,r3,d4,d3,d4,d5,d6,r7,d8,d9,d10,d11][...]]
			 */
			/*
			 * for(int a=1;a<=8;a++) {
			 * 
			 * }
			 */

			hamEncode[i][0] = (in[i][0] + in[i][2] + in[i][4] + in[i][6]) % 2;
			hamEncode[i][1] = (in[i][1] + in[i][2] + in[i][5] + in[i][6]) % 2;
			hamEncode[i][3] = (in[i][3] + in[i][4] + in[i][5] + in[i][6]) % 2;
			hamEncode[i][7] = (in[i][7]) % 2;

			for (int k = 0; k <= 7; k++) {
				switch (k) {
				case 0:
					hamEncode[i][2] = in[i][k];
					break;
				case 1:
					hamEncode[i][4] = in[i][k];
					break;
				case 2:
					hamEncode[i][5] = in[i][k];
					break;
				case 3:
					hamEncode[i][6] = in[i][k];
					break;
				case 4:
					hamEncode[i][8] = in[i][k];
					break;
				case 5:
					hamEncode[i][9] = in[i][k];
					break;
				case 6:
					hamEncode[i][10] = in[i][k];
					break;
				case 7:
					hamEncode[i][11] = in[i][k];
					break;
				default:
					break;
				}
			}
		}

		/* Comment out after implementation */
		// System.out.printf("r0: %d\nr1: %d\nr3: %d\nr8: %d\n", r0,r1,r3,r7);

		// System.out.print("Hamming: ");

		return hamEncode;
	}

	public static int[][] detectFix(int[][] en) {
		/*
		 * r0 -> r7 are passed into this method with en[][] Method to examine
		 * sent data and check for errors and associated positions
		 */
		// check for parity bits, if error detected, fix
		int errSum = 0;
		int[][] parity = new int[en.length][12];

		for (int i = 0; i < en.length; i++) {
			/*
			 * Hard coded Way. Check math. Make separate method maybe? Make
			 * better in future Math is maybe out here.
			 */
			parity[i][0] = (en[i][2] + en[i][4] + en[i][6] + en[i][8]) % 2;
			parity[i][1] = (en[i][4] + en[i][5] + en[i][9] + en[i][10]) % 2;
			parity[i][3] = (en[i][6] + en[i][8] + en[i][9] + en[i][10]) % 2;
			parity[i][7] = (en[i][11]) % 2;
			System.out.print(Arrays.deepToString(parity) + "\n");
			/*
			 * Assign the values form the in matrix to the parity checking
			 * matrix skipping the parity bits
			 */
			for (int k = 0; k <= 11; k++) {
				switch (k) {
				case 0:
				case 1:
				case 3:
				case 7:
					if (parity[i][k] != en[i][k]) {
						errSum += (k + 1);
					}
					break;
				default:
					parity[i][k] = en[i][k];
					break;
				}
			}
			/*
			 * This is only pseudo error fixing Doesn't really check where the
			 * error is.
			 */
			if (errSum <= 1) {
				if (parity[i][errSum] != en[i][errSum]) {
					if (en[i][errSum] == 1) {
						en[i][errSum] = 0;
					} else {
						en[i][errSum] = 1;
					}
				}
			}
		}
		/**/
		return parity;
	}

	public static int[][] decode(int[][] par) {
		int[][] base = new int[par.length][8];
		for (int i = 0; i < par.length; i++) {
			for (int k = 0; k <= 11; k++) {
				switch (k) {
				case 0:
					base[i][k] = par[i][2];
					break;
				case 1:
					base[i][k] = par[i][4];
					break;
				case 2:
					base[i][k] = par[i][5];
					break;
				case 3:
					base[i][k] = par[i][6];
					break;
				case 4:
					base[i][k] = par[i][8];
					break;
				case 5:
					base[i][k] = par[i][9];
					break;
				case 6:
					base[i][k] = par[i][10];
					break;
				case 7:
					base[i][k] = par[i][11];
					break;
				default:
					break;
				}
			}
		}
		return base;
	}

	public static String english(int[][] returned) {
		String word = "", sentence = "";
		for (int i = 0; i < returned.length; i++) {
			int letter = 0;
			for (int j = 0; j < returned[i].length; j++) {
				letter += (int) ((Math.pow(2, (7 - j)) * (returned[i][j])));
			}
			word += (char) letter;
		}
		sentence += word + " ";
		return sentence;
	}
}
