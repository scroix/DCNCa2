#!/bin/bash

if [ -d "$dateServer/bin" ]; then
  # Control will enter here if $DIRECTORY exists.
  rm -rf dateServer/bin
fi
mkdir dateServer/bin  
chmod 775 dateServer/bin  
javac dateServer/src/Client.java -cp dateServer/src/ -d dateServer/bin/  
javac dateServer/src/Server.java -cp dateServer/src/ -d dateServer/bin/  
cd dateServer/bin  
xterm -title "Server" -e "java Server" &  
sleep 3  
xterm -title "Client" -e "java Client" &  

